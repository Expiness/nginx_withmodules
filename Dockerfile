FROM debian:10 as build 

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.25.4.tar.gz && tar xvfz nginx-1.25.4.tar.gz && cd nginx-1.25.4 && ./configure && make && make install

FROM debian:10
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
